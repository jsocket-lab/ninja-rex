var express = require("express")
var app = express()
var http = require('http').createServer(app)
var io = require('socket.io')(http)

//makes the code public
app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
})

app.use('/', (req, res) => {
  res.sendFile(__dirname + "/public/index.html")
})

//socket connection

//var declarations

//clients are the connected screens
var clients = []
var nClients = 0
var screenSizeTot = 0
var lastScreen = 0

function Client(screenID, socketID, screenSize) {
  this.screenID = screenID;
  this.socketID = socketID;;
  this.screenSize = screenSize;
}

//on new screen connection
io.on('connection', function (socket) {
  console.log('A new screen has connected!')
  nClients += 1
  socket.emit('welcome', { clientNum: nClients })

  socket.on('welcomed', msg => {
    console.log('thank you for the info screen ', nClients)
    screenSizeTot += msg.screenSize
    lastScreen = nClients
    console.log('new screensize', screenSizeTot)
    clients.push(new Client(nClients, socket.id, msg.screenSize))
    io.emit('updateScreen', { screenSizeTot: screenSizeTot, lastScreen: lastScreen })
  })

  socket.on('pleaseUpdate', msg => {
    io.emit('updateObjPosition', msg)
  })

  socket.on('playStatus', msg => {
    io.emit('playStatus', msg)
  })

  socket.on('gameOver', msg => {
    io.emit('gameOver', msg)
  })

  socket.on('updateScore', msg => {
    io.emit('updateScore', msg)
  })

  socket.on('restart', msg => {
    io.emit('restart', msg)
  })

  socket.on('startGame', msg => {
    let list = []
    let players = msg.p
    let sn = msg.screenNumb

    clients.forEach((c) => {
      list.push(c.screenSize)
    })
    io.emit('startGame', { p: players, screenNumb: sn, screens: list })
  })

  socket.on('updateStatus', msg => {
    io.emit('updateDinoStatus', msg)
  })

  socket.on('updateDeath', msg => {
    io.emit('updateDinoDeath', msg)
  })

  socket.on('sunStatus', msg => {
    io.emit('sunStatus', msg)
  })

  socket.on('updateHI', msg => {
    io.emit('updateHI', msg)
  })

  socket.on('changeMode', msg => {
    io.emit('changeMode', msg)
  })

  socket.on('animeGigarex', msg => {
    io.emit('animeGigarex', msg)
  })

  socket.on('disconnect', msg => {
    console.log('disconnect')
    nClients -= 1
    lastScreen = nClients
    clients.forEach(function (client) {
      if (client.socketID == socket.id) {
        console.log('client ', client.screenID, 'disconnected')
        screenSizeTot -= client.screenSize
        console.log('new screensize', screenSizeTot)
        clients.pop(client)
      }
    })
    io.emit('updateScreen', { screenSizeTot: screenSizeTot })
  })

  socket.on('updateRankStatus', msg => {
    io.emit('updateRankStatus', msg)
  })

})

//http connection to port
http.listen(8116, function () {
  console.log('listening on port 8116')
})
