import Dino from './Dino.js'

var avColors = [0, 1, 2, 3]

export function addPlayer(fauxPlayers, screenH, control){
    if(avColors.length == 0){
        avColors = [0, 1, 2, 3]
    }
    var aux = 0
    var imgW = 0
    var lastX = 0
    var x = 0
    var rdm = Math.floor(Math.random() * 4)
    fauxPlayers.forEach(dino => {
        if (dino.control == control) {
            fauxPlayers = removePlayer(fauxPlayers, dino)
            aux++;
        }
        if(dino.color == rdm){
            rdm = avColors[Math.floor(Math.random() * avColors.length)]
        }
    })
    if (aux == 0) {
        if (fauxPlayers.length == 0) {
            fauxPlayers.push(new Dino(0, screenH / 2, control, rdm))
        }
        else {
            for(var i = 0 ; i < fauxPlayers.length; i++){
                if(fauxPlayers[i].posX > x){
                    fauxPlayers[i].posX = x
                }
                imgW = fauxPlayers[i].img.width
                lastX = fauxPlayers[i].posX
                x = (imgW + lastX)
            }
            fauxPlayers.push(new Dino(x, screenH / 2, control, rdm))
        }
        avColors.splice(avColors.indexOf(rdm), 1)
        avColors = [...avColors]
    }
    aux = 0
    return fauxPlayers
}

function removePlayer(fauxPlayers, dino){
    avColors.push(dino.color)
    fauxPlayers.splice(fauxPlayers.indexOf(dino), 1)
    fauxPlayers = [...fauxPlayers]
    return fauxPlayers
}

export function jumpDown(dino){
    if(!dino.dead && dino.posY == dino.iposY){
        dino = jumpKey(dino)
    }
    return dino
}

function jumpKey(dino) {
    if (!dino.jumping) {
        dino.posY = dino.iposY
        dino.ducking = false
        dino.walking = false
        dino.jumping = true
    }
    return dino
}

export function duckDown(dino){
    if(!dino.dead && dino.posY == dino.iposY && !dino.jumping){
        dino = duckKey(dino)
    }
    return dino
}

function duckKey(dino) {
    if (dino.posY == dino.iposY && !dino.jumping) {
        dino.walking = false
        dino.ducking = true
        dino.jumping = false
    }
    return dino
}

export function readyDino(dino){
    if(dino.posY == dino.iposY){
        dino.ready = !dino.ready
        dino.walking = !dino.walking
    }
    return dino
}

export function jumpUp(dino){
    dino.walking = true
    dino.jumping = false
    dino.ducking = false
    return dino
}

export function duckUp(dino){
    dino.walking = true
    dino.ducking = false
    dino.jumping = false
    return dino
}

export function changeletter(letter){
    if(letter != "Z"){
        letter = String.fromCharCode(letter.charCodeAt()+1)
    }
    else
        letter = "A"
    return letter
}