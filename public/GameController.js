import Game from './Game.js'
import Dino from './Dino.js'
import Score from './Score.js'
import Ptero from './Ptero.js'
import * as drawFunc from './DrawFunctions.js'
import * as controlFunc from './ControlFunctions.js'
import GigaRex from './Gigarex.js'

//Sockets
var socket = io()
var screenRes = window.innerWidth
var screenNumber = 0
var lastScreen = 0
var previousScreen = 0
var previusObstacle = ""
var scorePs = []
var hi = ""
var anime = null
var image = null
var newPosY = 0
var newPosX = 0
var newImg = -1
var newTime = 0

var currentSun = false

// Screen
var screenH = window.innerHeight
var screenW = window.innerWidth

//Layer Backgroun and Obstacles
var img = document.getElementById('bg')
var canvas = document.getElementById('canvas')
var ctx = canvas.getContext('2d')
canvas.width = screenW
canvas.height = screenH

//Layer Players
var layerPlayer = document.getElementById('players')
var ctxP = layerPlayer.getContext('2d')
layerPlayer.width = screenW
layerPlayer.height = screenH

//Layer Score
var layerScore = document.getElementById('score')
var ctxS = layerScore.getContext('2d')
layerScore.width = screenW
layerScore.height = screenH


//Sound
var sound1 = document.getElementById('sound1')
var sound2 = document.getElementById('sound2')

//Player
var fauxPlayers = []

//Background
var distance = 0
var numImages = Math.ceil(screenRes / img.width) + 1
var backgroundColor = 'rgba(0, 0, 0, 0)'
let alpha = 0
let change = null

let timeGameOver

let game
let score

//Rank
let playerRecord = null
let pos = 0
let auxRank = null
let animationRank = null


document.addEventListener('load', newGame())


/**
* newGame()
* Instantiates a new game
* Calls menu
* Inits the game
*/
function newGame() {
    game = new Game()
    score = new Score()
    fauxPlayers = []
    menu()
    loop()

    turnOnSockets()
}

/**
 * restart()
 * When gameOver is true, restart is called 
 * to clean the game and start it again
 */
function restart() {
    newPosX = 0
    if (game.nightMode) {
        backgroundColor = 'rgba(0, 0, 0, 0)'
        alpha = 0
    }
    if (game.upsideDownMode) {
        flip()
    }

    score.gameScore = []
    playerRecord = null
    pos = 0

    game.reset()
    fauxPlayers = []
    if (screenNumber == 1) {
        socket.emit('updateHI', { hi: score.hiscore })
    }

    menu()
    loop()
}

/**
 * turnSocketOn
 */
function turnOnSockets() {
    //welcomes the new screen
    socket.on('welcome', msg => {
        screenNumber = msg.clientNum
        socket.emit('welcomed', { screenSize: screenRes })
    })

    //update screen size
    socket.on('updateScreen', msg => {
        screenRes = msg.screenSizeTot
        lastScreen = msg.lastScreen
    })


    //update objects position on GAME
    socket.on('updateObjPosition', msg => {
        previousScreen = msg.screenNumb
        previusObstacle = msg.obstacleType
        newPosY = msg.posY
        newImg = msg.index
        newTime = new Date(msg.timeRoar)
        anime = msg.anime
    })

    //play status
    socket.on('playStatus', msg => {
        if (screenNumber != 1)
            game.play = true
    })

    //updates score
    socket.on('updateScore', msg => {
        if (screenNumber == lastScreen) {
            scorePs = msg.listP
        }
    })

    //game over
    socket.on('gameOver', msg => {
        cancelAnimationFrame(game.time)
        gameOver()
        sound1.pause()
        sound2.currentTime = 9
        sound2.play()
    })

    //restart
    socket.on('restart', msg => {
        sound2.pause()
        restart()
    })

    //test
    socket.on('startGame', msg => {
        sound1.currentTime = 7
        sound1.play()
        if (screenNumber != 1) {
            for (var i = screenNumber - 1; i > 0; i--) {
                newPosX -= msg.screens[i - 1]
            }
            msg.p.forEach(p => {
                var dino = new Dino(newPosX, screenH / 2, p.control, p.color)
                dino.score = p.score
                dino.ducking = p.ducking
                dino.dead = p.dead
                dino.jumping = p.jumping
                dino.walking = p.walking
                dino.ready = p.ready
                dino.animCount = p.animCount
                dino.changeAnim = p.changeAnim
                dino.iposY = p.iposY
                dino.posY = p.posY
                game.addPlayer(dino)
            })
        }
        game.timeGame = new Date()
    })

    //
    socket.on('updateDinoStatus', msg => {
        if (screenNumber != 1) {
            game.players.forEach(p => {
                if (p.control == msg.dino.control) {
                    p.posX = newPosX + msg.dino.posX
                    p.ducking = msg.dino.ducking
                    p.dead = msg.dino.dead
                    p.jumping = msg.dino.jumping
                    p.walking = msg.dino.walking
                    p.ready = msg.dino.ready
                    p.animCount = msg.dino.animCount
                    p.changeAnim = msg.dino.changeAnim
                }
            })
        }
    })

    //
    socket.on('updateDinoDeath', msg => {
        if (screenNumber == 1) {
            game.players.forEach(p => {
                if (p.control == msg.control) {
                    p.dead = true
                    p.walking = false
                    p.jumping = false
                    p.ducking = false
                }
            })
        }
    })

    //
    socket.on('sunStatus', msg => {
        currentSun = msg.status
    })

    //
    socket.on('updateHI', msg => {
        hi = msg.hi
    })

    //
    socket.on('changeMode', msg => {
        if (msg.change == "nightMode")
            game.nightMode = msg.mode
        else
            game.upsideDownMode = msg.mode
        change = msg.change
    })

    //
    socket.on('animeGigarex', msg => {
        if (screenNumber == msg.screenNumber + 1) {
            image = msg.img
        }
    })

    //control rank
    socket.on('updateRankStatus', msg => {
        auxRank = msg.rank
        game.rank = msg.gameRank
        if (screenNumber != 1){
            playerRecord = msg.playerRecord
            pos = msg.pos
        }     

    })

}

/**
 * verify()
 * Watches if all players are ready
 * If true, starts the game
 */
function verify() {
    var count = 0

    fauxPlayers.forEach(dino => {
        if (dino.ready) {
            count++;
        }
    })
    if (fauxPlayers.length == count && fauxPlayers.length != 0) {
        fauxPlayers.forEach(p => {
            p.iposX = 0
        })
        game.init(fauxPlayers, screenH / 2)
        game.play = true
        socket.emit('startGame', { p: fauxPlayers })
        socket.emit('playStatus', { play: true })
        if (screenNumber == 1) {
            socket.emit('updateHI', { hi: score.hiscore })
        }
    }
}

/**
 * menu()
 * Draws the menu screen
 * Calls loop function
 */

function menu() {
    ctx.clearRect(0, 0, screenW, screenH)
    ctxS.clearRect(0, 0, screenW, screenH)
    ctxP.clearRect(0, 0, screenW, screenW)
    ctx.save()
    ctx.fillStyle = 'white';
    //draws the background
    background(0)
    if (screenNumber != Math.round(lastScreen / 2)) {
        drawFunc.drawText(ctx, screenW, screenH, 0, game.nightMode)
    }
    rank()


    if (screenNumber == 1) {
        if (fauxPlayers.length != 0) {
            fauxPlayers.forEach(dino => {
                if (dino.iposY != undefined)
                    if (dino.posY < dino.iposY) {
                        dino.posY += 5;
                    }
                    else {
                        dino.posY = dino.iposY;
                    }
                ctx.drawImage(dino.animation(), dino.posX, dino.posY)
            })
        }
    }

    ctx.restore()
}

/**
 * background()
 * Controls the background
 */
function background(velocity) {
    distance -= velocity
    if (distance < -img.width) {
        distance = 0
    }
    ctx.fillStyle = backgroundColor
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.save()
    ctx.translate(distance, 0)
    for (var i = 0; i < numImages; i++) {
        ctx.drawImage(img, i * img.width, screenH / 2)
    }
    ctx.restore()

}

/**
 * draw()
 * Draws the game
 */
function draw() {
    ctx.clearRect(0, 0, screenW, screenH)
    ctxS.clearRect(0, 0, screenW, screenH)
    ctxP.clearRect(0, 0, screenW, screenW)
    ctxS.save()
    ctxP.save()



    //draws the background
    background(game.vel)


    if (screenNumber == lastScreen && lastScreen != 1) {
        drawFunc.drawHI(ctxS, hi, 4, game.nightMode)
    } else if (screenNumber == lastScreen && lastScreen == 1) {
        drawFunc.drawHI(ctxS, hi, 4, game.nightMode)
    }

    var scoreY = 25
    scorePs.forEach(p => {
        //Player Score
        if (screenNumber == lastScreen) {
            drawFunc.drawScore(ctxS, screenW, p.score, scoreY)
            scoreY += 25
        }
    })

    game.players.forEach(p => {
        ctxP.drawImage(p.animation(), p.posX, p.posY)
    })

    game.obstacles.forEach(o => {
        ctx.drawImage(o.img, o.x, o.y)
    })

    game.sky.forEach(s => {
        ctx.drawImage(s.img, s.x, s.y)
    })

    ctxP.restore()
    ctxS.restore()
}

/**
 * loop()
 * Creates loop for the game
 * Calls the update function to update game variables
 *
 */
function loop() {
    var auxDeath = 0
    if (!game.play) {
        score.getHI()
        menu()
        window.requestAnimationFrame(loop)
    } else {
        score.getHI()
        draw()
        game.players.forEach(p => {
            if (p.dead) {
                auxDeath++
                score.gameScore.push(p.score.score)
            }
        })
        if (game.players.length == auxDeath) {
            auxDeath = 0

            if (screenNumber == 1)
                socket.emit('gameOver', { gameStat: "end" })
        }
        else {
            game.time = window.requestAnimationFrame(loop)
        }
    }
    auxDeath = 0
    update()
}

/**
 * gameOver()
 * Draws game over screen
 * Sets the end of the game
 * Plays sad song :(
 */
function gameOver() {
    draw() //draws last frame
    cancelAnimationFrame(game.time)
    score.hiScore()
    game.gameOver = true
    game.play = false
    timeGameOver = new Date()

    //Game Over
    if (screenNumber != Math.round(lastScreen / 2)) {
        drawFunc.drawText(ctx, screenW, screenH, 2, game.nightMode)
    }
    rank()

    game.time = 0
}

/**
 * update()
 * Sends information to all the screens
 * Makes background and obstacles animation
 */
function update() {
    var listPScore = []
    if (game.play) {
        game.players.forEach(p => {
            p.move()
            if (screenNumber == 1) {
                if (!p.dead) {
                    p.score.changeScore()
                }
                listPScore.push(p.score)
                socket.emit('updateScore', { listP: listPScore })
                socket.emit('updateStatus', { dino: p })
            }
            if (p.posX > (screenW * (lastScreen - screenNumber + 1)) - p.w) {
                p.posX = p.iposX
            }

            if (p.jumping) {
                p.jump()
            }
            else if (p.ducking && !p.jumping) {
                p.duck()
            }
            else if (p.dead) {
                p.posX = p.posX - game.vel
                if (p.posX < 0 && game.players.length != 1) {
                    game.players.splice(game.players.indexOf(p), 1)
                    game.players = [...game.players]
                }
            }

            if (game.obstaclesMode) {
                game.obstacles.forEach(element => {
                    if (p.collider(element) && !(element instanceof GigaRex)) {
                        p.walking = false
                        p.jumping = false
                        p.ducking = false
                        p.dead = true
                        socket.emit('updateDeath', { control: p.control })
                    }
                    if (element instanceof GigaRex) {
                        if (element.collider(p)) {
                            p.walking = false
                            p.jumping = false
                            p.ducking = false
                            p.dead = true
                            socket.emit('updateDeath', { control: p.control })
                        }
                    }
                });


            }
        })

        //Instantiate obstacles
        if (game.startObstacles) {
            game.timer.obstacles = new Date()
            game.startObstacles = false
        }

        let now = new Date()
        let delta = now - game.timer.obstacles

        if (delta > game.timeObstacles) {
            let obstacles = Math.floor(Math.random() * (4 - 1 + 1)) + 1
            if (screenNumber == lastScreen) {
                switch (obstacles) {
                    case 1:
                        game.addSmallCactus(screenW, screenH / 2, -1)
                        game.timer.obstacles = now
                        break;
                    case 2:
                        game.addBigCactus(screenW, screenH / 2, -1)
                        game.timer.obstacles = now
                        break;

                    case 3:
                        game.addCactus(screenW, screenH / 2)
                        game.timer.obstacles = now
                        break;
                    case 4:
                        game.addPtero(screenW, screenH / 2, false)
                        game.timer.obstacles = now
                        break;
                }
                //Create Sky
                game.addCloud(screenW, screenH / 2, false)
                game.timer.obstacles = now

                //if sun doesn't exist, create
                if (!currentSun) {
                    game.addSun(screenW, screenH / 2, false);
                    game.timer.obstacles = now
                    socket.emit('sunStatus', { status: true })
                }


            }
        }
        //copy obstacle
        if (screenNumber == previousScreen - 1) {
            genObstacle(previusObstacle)
            previousScreen = -1
        }
        if (screenNumber == previousScreen + 1 && previusObstacle == 'gigaRex' && !game.gigarex) {
            game.addGigarex(screenH / 2, newTime, anime)
            game.gigarex = true
        }


        //Move Sky
        game.sky.forEach(s => {
            if (s.getType == 'sun')
                s.move(0.5)
            else
                s.move(game.vel - 3);
            if (s.getX + s.width < -(s.img.width)) {
                if (screenNumber == 1 && s.getType == 'sun') {
                    socket.emit('sunStatus', { status: false })
                }
                game.removeSky(s)
            }
            if (s.getX < 0 && s.dead == false) {
                s.dead = true
                socket.emit('pleaseUpdate', { screenNumb: screenNumber, obstacleType: s.type, posY: s.getY })
            }
        })

        //Move obstacles
        game.obstacles.forEach(o => {
            if (o instanceof GigaRex) {
                if (o.getX + o.width > screenW && o.img.src != image) {
                    socket.emit('animeGigarex', { screenNumber: screenNumber, img: o.img.src })
                }
                if (image != null) {
                    o.img.src = image
                } else {
                    o.anime()
                }

                o.move(0.5)
                if (o.getX + o.width > screenW && o.dead == false) {
                    o.dead = true
                    socket.emit('pleaseUpdate', { screenNumb: screenNumber, obstacleType: o.type, posY: o.getY, index: o.imgIndex, timeRoar: o.timeRoar, anime: o.changeAnime })
                }
                if (o.getX > screenW) {
                    socket.emit('animeGigarex', { screenNumber: screenNumber, img: null })
                    game.removeObstacles(o)
                    game.gigarex = false
                }

            }
            else {
                o.move(game.vel)
                if (o instanceof Ptero)
                    o.anime()

                if (o.getX + o.width < -(o.img.width) && !(o instanceof GigaRex))
                    game.removeObstacles(o)

                if (o.getX < 0 && o.dead == false) {
                    o.dead = true
                    socket.emit('pleaseUpdate', { screenNumb: screenNumber, obstacleType: o.type, posY: o.getY, index: o.imgIndex })
                }
            }
        })


        game.difficulty()

        //Timer feature
        if (screenNumber == 1) {
            now = Number(listPScore[0].score.number)
            delta = now - game.timer.nightMode
            if (delta > 500) {
                game.nightMode = !game.nightMode
                socket.emit('changeMode', { mode: game.nightMode, change: "nightMode" })
                game.timer.nightMode = now
            }
            delta = now - game.timer.upsideDownMode
            if (delta > 1500) {
                game.upsideDownMode = !game.upsideDownMode
                socket.emit('changeMode', { mode: game.upsideDownMode, change: "upsideDownMode" })
                game.timer.upsideDownMode = now

            }
            delta = now - game.timer.gigarex
            //GigaRex
            if (delta > 3000 && !game.gigarex) {
                game.addGigarex(screenH / 2, newTime, anime)
                game.gigarex = true
                game.timer.gigarex = now
            }
        }

        // night mode smoothing
        if (alpha < 1 && game.nightMode) {
            alpha += 0.01
            backgroundColor = `rgba(0, 0, 0, ${alpha})`
        } else if (alpha > 0 && !game.nightMode) {
            alpha -= 0.01
            backgroundColor = `rgba(0, 0, 0, ${alpha})`

        }

        switch (change) {
            case "nightMode":
                if (game.nightMode) {
                    ctxS.globalCompositeOperation = 'difference'
                } else {
                    ctxS.globalCompositeOperation = 'source-over'
                }
                change = null
                break;
            case "upsideDownMode":
                flip()
                change = null
                break;
        }

    }
}

function genObstacle(previusType) {
    switch (previusType) {
        case 'bigcactus':
            game.addBigCactus(screenW, screenH / 2, newImg)
            break;
        case 'cactus':
            game.addCactus(screenW, screenH / 2)
            break;
        case 'smallcactus':
            game.addSmallCactus(screenW, screenH / 2, newImg)
            break;
        case 'ptero':
            game.addPtero(screenW, newPosY, true)
            break;
        case 'cloud':
            game.addCloud(screenW, newPosY, true)
            break;
        case 'sun':
            game.addSun(screenW, newPosY, true)
            break;
    }
}

/**
 * controlListener()
 * Controllers
 */
var allControllers = [
    { number: 1, up: 'ArrowUp', down: 'ArrowDown' },
    { number: 2, up: 'w', down: 's' },
    { number: 3, up: 'u', down: 'j' },
    { number: 4, up: 'o', down: 'l' }
]
document.addEventListener('keydown', function (event) {
    if (event.defaultPrevented) {
        return
    }

    var key = event.key

    allControllers.forEach(c => {
        if (game.play) {
            game.players.forEach(dino => {
                if (dino.control == c.number) {
                    if (!game.upsideDownMode) {
                        if (key === c.up) {
                            dino = controlFunc.jumpDown(dino)
                        }
                        else if (key === c.down) {
                            dino = controlFunc.duckDown(dino)
                        }
                    } else {
                        if (key === c.up) {
                            dino = controlFunc.duckDown(dino)
                        }
                        else if (key === c.down) {
                            dino = controlFunc.jumpDown(dino)
                        }
                    }

                }
            })
        }
        else {
            if (key === c.up) {
                if (game.gameOver && !game.rank) {
                    let now = new Date()
                    let delta = now - timeGameOver
                    if (delta > 3000) {
                        socket.emit('restart', true)
                    }
                } else if (game.rank) {
                    pos++
                    if (pos > 2) {
                        score.updateScore()
                        game.rank = false
                        cancelAnimationFrame(animationRank)
                    }
                    socket.emit('updateRankStatus', { rank: score.rank, playerRecord: playerRecord, gameRank: game.rank, pos: pos })
                }
                else {
                    fauxPlayers = controlFunc.addPlayer(fauxPlayers, screenH, c.number)
                }
            }
            else if (key === c.down) {
                if (!game.gameOver && !game.rank) {
                    fauxPlayers.forEach(dino => {
                        if (dino.control == c.number) {
                            dino = controlFunc.readyDino(dino)
                        }
                    })
                    verify()
                } else if (game.rank) {
                    playerRecord.letters[pos] = controlFunc.changeletter(playerRecord.letters[pos])
                    socket.emit('updateRankStatus', { rank: score.rank, playerRecord: playerRecord, gameRank: game.rank, pos: pos })
                }
            }
        }
    })
})

document.addEventListener('keyup', function (event) {
    if (event.defaultPrevented) {
        return
    }

    var key = event.key

    allControllers.forEach(c => {
        if (game.play) {
            game.players.forEach(dino => {
                if (dino.control == c.number) {
                    if (!game.upsideDownMode) {
                        if (key === c.up) {
                            dino = controlFunc.jumpUp(dino)
                        }
                        else if (key === c.down) {
                            dino = controlFunc.duckUp(dino)
                        }
                    } else {
                        if (key === c.up) {
                            dino = controlFunc.duckUp(dino)
                        }
                        else if (key === c.down) {
                            dino = controlFunc.jumpUp(dino)
                        }
                    }

                }
            })
        }
    })
})

function flip() {
    ctxP.clearRect(0, 0, screenW, screenW)
    ctx.translate(0, screenH)
    ctx.scale(1, -1)
    ctxP.translate(0, screenH)
    ctxP.scale(1, -1)
}

function rank() {
    //Update Rank
    if (screenNumber == 1) {
        if (game.gameOver) {
            score.gameScore.sort(function (a, b) {
                if (a.number < b.number) {
                    return 1;
                }
                if (a.number > b.number) {
                    return -1;
                }
                return 0;
            });
            if (score.newRecord(score.gameScore[0])) {
                game.rank = true
                playerRecord = score.gameScore[0]
            }

        }
        socket.emit('updateRankStatus', { rank: score.rank, playerRecord: playerRecord, gameRank: game.rank, pos: pos })
    }
    if (screenNumber == Math.round(lastScreen / 2)) {
        if (auxRank != null && auxRank != undefined)
            drawFunc.drawRank(ctxS, auxRank, screenW, screenH, 0, game.nightMode)
        if (game.gameOver) {
            ctxS.clearRect(0, 0, screenW, screenH)
            ctx.save()
            if (auxRank != null && auxRank != undefined)
                drawFunc.drawRank(ctxS, auxRank, screenW, screenH, 0, game.nightMode)
            if (playerRecord != null && playerRecord != undefined) {
                drawFunc.drawNewRecord(ctxS, playerRecord, screenW, screenH, 0, pos, game.nightMode)
            }
            ctxS.restore()
            animationRank = window.requestAnimationFrame(rank)
        }

    }
}