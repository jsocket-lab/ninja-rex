import Obstacle from "./Obstacles.js";

var ImgCactos = [
    'assets/cactus1.png',
    'assets/cactus2.png',
    'assets/cactus3.png',
    //'assets/cactus4.png',
    'assets/cactus5.png',
    //'assets/2smallcactus.png'
]

export default class SmallCactus extends Obstacle {



    constructor(x, y, newImg) {
        super()
        if (newImg == -1) {
            this.imgIndex = Math.floor(Math.random() * (ImgCactos.length - 0) + 0)
        } else {
            this.imgIndex = newImg
        }
        this.setImgSrc = ImgCactos[this.imgIndex]

        this.setType = 'smallcactus'
        this.setX = x
        //this.y = y - this.getHeight*0.7
        const imgPromise = new Promise((resolve, reject) => {
            var auxImg = new Image()
            auxImg.src = ImgCactos[0]
            auxImg.addEventListener("load", function () {
                if (auxImg.height != 0) {
                    resolve(auxImg.height)
                }
                else {
                    reject(auxImg.height)
                }
            })
        })
        imgPromise.then(result => {
            this.setY = (y - (result * 0.7));
        })
    }


}