
export default class Score {
    //hiscore = {}
    rank = [
        { name: "---", score: "-----" },
        { name: "---", score: "-----" },
        { name: "---", score: "-----" },
        { name: "---", score: "-----" },
        { name: "---", score: "-----" }]

    gameScore = []

    lastupdate = new Date()

    change = true

    constructor() {
        this.score = { number: "00000", color: null, letters: ["A", "A", "A"] }
    }

    getHI() {
        if (localStorage != null && localStorage.length > 0) {
            this.rank = JSON.parse(localStorage.getItem("rank"))
            this.hiscore = this.rank[0].score
        }
        else {
            this.hiscore = "00000"
        }
    }

    changeScore() {
        let now = new Date();
        let delta = now - this.lastupdate

        if (delta > 100) {
            var num = parseInt(this.score.number)
            num += 1
            if (num <= 9) {
                this.score.number = `0000${JSON.stringify(num)}`
            }
            else if (num <= 99) {
                this.score.number = `000${JSON.stringify(num)}`
            }
            else if (num <= 999) {
                this.score.number = `00${JSON.stringify(num)}`
            }
            else if (num <= 9999) {
                this.score.number = `0${JSON.stringify(num)}`
            }
            else {
                this.score.number = JSON.stringify(num)
            }
            this.lastupdate = now
        }
    }


    hiScore() {
        this.hiscore = this.rank[0].number
    }

    updateScore(){
        for (var i = 0; i < this.gameScore.length; i++) {
            for (var j = 0; j < this.rank.length; j++) {
                //Rank empty
                if (this.rank[j].score == "-----" && this.change) {
                    this.rank[j].name = this.gameScore[i].letters.join('')
                    this.rank[j].score = this.gameScore[i].number
                    this.change = false
                }
                if (Number(this.gameScore[i].number) > Number(this.rank[j].score) && this.change) {
                    this.rank.splice(j, 0, { name: this.gameScore[i].letters.join(''), score: String(this.gameScore[i].number) })
                    this.change = false
                }
                if (this.rank.length > 5) {
                    this.rank.pop()
                }
            }
        }

        localStorage.setItem("rank", JSON.stringify(this.rank))
        this.change = true
    }

    newRecord(player) {
        for (var j = 0; j < this.rank.length; j++) {
            if((this.rank[j].score == "-----" && this.change) || (Number(player.number) > Number(this.rank[j].score) && this.change))
                return true
        }
        return false
    }
}