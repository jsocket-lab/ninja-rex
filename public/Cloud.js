import Sky from "./Obstacles.js"

var cloud = "assets/cloud.png"

export default class Cloud extends Sky {
    constructor(x, y, cp) {
        super()
        this.setImgSrc = cloud
        this.setType = 'cloud'
        this.setX = x

        if (cp) {
            this.setY = y
        }
        else {
            const imgPromise = new Promise((resolve, reject) => {
                var auxImg = new Image()
                auxImg.src = cloud
                auxImg.addEventListener("load", function () {
                    if (auxImg.height != 0) {
                        resolve(auxImg.height)
                    }
                    else {
                        reject(auxImg.height)
                    }
                })
            })
            imgPromise.then(result => {
                this.randomHeight(y, result)

            })
        }
    }

    randomHeight(y, result) {
        let random = (Math.floor(Math.random() * (10 - 1 + 1)) + 1) % 3
        switch (random) {
            case 0:
                this.y = (y - (result * 7))
                break;
            case 1:
                this.y = (y - (result * 8))
                break;
            case 2:
                this.y = (y - (result * 10))
                break;
        }

    }
}