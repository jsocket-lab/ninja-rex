
const cache_v='v1'
const cache_files=[
'/',
'/index.html',
'assets/walk1-yellow.png',
'assets/walk2-yellow.png',
'assets/duck1-yellow.png',
'assets/duck2-yellow.png',
'assets/up-yellow.png',
'assets/end-yellow.png',
'assets/walk1-red.png',
'assets/walk2-red.png',
'assets/duck1-red.png',
'assets/duck2-red.png',
'assets/up-red.png',
'assets/end-red.png',
'assets/walk1-blue.png',
'assets/walk2-blue.png',
'assets/duck1-blue.png',
'assets/duck2-blue.png',
'assets/up-blue.png',
'assets/end-blue.png',
'assets/walk1-green.png',
'assets/walk2-green.png',
'assets/duck1-green.png',
'assets/duck2-green.png',
'assets/up-green.png',
'assets/end-green.png',
'assets/cactus1.png',
'assets/cactus2.png',
'assets/cactus3.png',
'assets/cactus4.png',
'assets/2bigcactus.png',
'assets/2smallcactus.png',
'assets/bigcactus1.png',
'assets/bigcactus2.png',
'assets/ptero1.png',
'assets/ptero2.png',
'assets/background.png',
'assets/music/start.mp3',
'assets/music/gameover.mp3',
'assets/walk1-3x.png',
'assets/walk2-3x.png',
'assets/up3x.png',
'assets/roar3x-1.png',
'assets/roar3x-2.png',
]



self.addEventListener('install',(e)=>{
	e.waitUntil(
		caches.open(cache_v)
		.then(cache=>{
			return cache.addAll(cache_files)		
		})
		.catch(err=>{
			throw Error(err)		
		})
	
	)
})



self.addEventListener('fetch',(e)=>{
	e.respondWith(
		caches.match(e.request)
		.then(res=>{
			if(res){
				return res
			}
			return fetch(e.request)		
		})
		.catch(err=>{throw Error(err)})		
	)
})

