import Score from './Score.js'

var colors = ["rgb(251,188,21)","rgb(232,67,54)","rgb(49,168,83)","rgb(81,123,189)"]
//dinosaur images
var jump = [
    {link: "assets/up-yellow.png"}, 
    {link: "assets/up-red.png"}, 
    {link: "assets/up-green.png"}, 
    {link: "assets/up-blue.png"}
]

var ooped = [
    {link: "assets/end-yellow.png"}, 
    {link: "assets/end-red.png"}, 
    {link: "assets/end-green.png"}, 
    {link: "assets/end-blue.png"}
]

var up = [
    {link: [
        "assets/walk1-yellow.png", 
        "assets/walk2-yellow.png"
    ]}, 
    {link: [
        "assets/walk1-red.png", 
        "assets/walk2-red.png"
    ]}, 
    {link: [
        "assets/walk1-green.png", 
        "assets/walk2-green.png"
    ]}, 
    {link: [
        "assets/walk1-blue.png", 
        "assets/walk2-blue.png"
    ]}
]

var down = [
    {link: [
        "assets/duck1-yellow.png", 
        "assets/duck2-yellow.png"
    ]}, 
    {link: [
        "assets/duck1-red.png", 
        "assets/duck2-red.png"
    ]}, 
    {link: [
        "assets/duck1-green.png", 
        "assets/duck2-green.png"
    ]}, 
    {link: [
        "assets/duck1-blue.png", 
        "assets/duck2-blue.png"
    ]}
]

//#region dino

export default class tRex{
    img = new Image();
    score = new Score();

    //dino status
    ducking = false;
    dead = false;
    jumping = false;
    walking = false;
    ready = false;

    //vel in y
    vy = 0

    //vel in x
    vx = 0

    //gravity
    gravity = 0.5
    //air resistence
    resistence = 0.1

    animCount = 0;
    changeAnim = false;
    
    h = 0
    w = 0

    constructor(x, y, control, rdm) {
        this.color = rdm
        this.img.src = ooped[rdm].link;
        this.control = control;
        this.score.score.color = colors[rdm];
        this.posX = x;
        this.iposX = x;
        this.posY = 0;
        const imgPromise = new Promise((resolve,reject)=>{
            var auxImg = new Image()
            auxImg.src = ooped[rdm].link
            auxImg.addEventListener("load", function(){
                if(auxImg.height != 0){
                    resolve(auxImg.height)
                }
                else{
                    reject(auxImg.height)
                }
            })
        })
        imgPromise.then(result => {
            this.iposY = (y - (result*0.75));
        })
    }

    animation() {
        var auxImg = new Image()
        if (this.animCount == 10) {
            this.changeAnim = !this.changeAnim;
            this.animCount = 0;
        }
        this.animCount++;

        if (this.walking) {
            if (this.changeAnim) {
                auxImg.src = up[this.color].link[0];
            }
            else {
                auxImg.src = up[this.color].link[1];
            }
        }
        else if(this.jumping){
            auxImg.src = jump[this.color].link;
        }
        else if(this.ducking){
            if(this.changeAnim){
                auxImg.src = down[this.color].link[0];
            }
            else {
                auxImg.src = down[this.color].link[1];
            }
        }
        else if(this.dead){
            auxImg.src = ooped[this.color].link;
        }
        else{
            auxImg.src = jump[this.color].link
        }

        this.h = this.img.height
        this.w = this.img.width

        auxImg.addEventListener("load", function(){
            this.img = auxImg
            
        })
        return auxImg
    }

    jump(){
        //let the dino jump only if its pos y is on the ground
        if(this.posY == this.iposY){
            //sets vel in y as -5 so it can go up little by little
            this.vy = -13
            this.vx = 4
        }
    }

    move(){
        if(this.dead){
            this.posX--;
        }
        else{
            //moves the dino up if vy is different than 0
            this.posY += this.vy

            //changes the vel so the dino can go back having gravity applied to it
            this.vy += this.gravity

            this.posX += this.vx

            this.vx -= this.resistence

            //to make the dino jump and stay in place
            //making the pos y be the min between the max of pos y and the height
            this.posY = Math.min(Math.max(this.posY, 0), this.iposY)

            this.posX = Math.max(this.posX, this.iposX)
        }
    }

    duck(){
        //if the dino is not ducking
        if(this.posY == this.iposY){
            //change its pos y to manage the image height
            this.posY -= (this.h - (1.25*this.h))

            this.vx = 2
        }
    }

    collider(obj) {
        return this.posX + 16 < obj.x + obj.getWidth &&
            this.posX + 16 + this.w*.562 > obj.x &&
            this.posY < obj.y + obj.getHeight &&
            this.posY + this.h*.869 > obj.y
    }
}

//#endregion
