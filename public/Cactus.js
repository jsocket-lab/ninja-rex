import Obstacle from "./Obstacles.js"

var cactus = "assets/cactus4.png"

export default class Cactus extends Obstacle {

    constructor(x, y) {
        super()
        this.setImgSrc = cactus
        this.setType = 'cactus'
        this.setX = x
        //this.y = y - this.getHeight*0.7
        const imgPromise = new Promise((resolve,reject)=>{
            var auxImg = new Image()
            auxImg.src = cactus
            auxImg.addEventListener("load", function(){
                if(auxImg.height != 0){
                    resolve(auxImg.height)
                }
                else{
                    reject(auxImg.height)
                }
            })
        })
        imgPromise.then(result => {
            this.setY = (y - (result*0.7));
        })
    }
}