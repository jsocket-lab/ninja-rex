import Obstacle from "./Obstacles.js"

var walk = [
    'assets/walk1-3x.png',
    'assets/walk2-3x.png'
]

var roar = [
    'assets/roar3x-2.png',
    'assets/roar3x-1.png'
]

export default class GigaRex extends Obstacle {

    changeAnime = true
    roar = false
    constructor(x, y, roar, anime) {
        super()
        this.setImgSrc = walk[0]
        this.setType = 'gigaRex'
        if (roar != 0) {
            this.timeRoar = roar
            this.changeAnime = anime
        }
        else {
            this.timeRoar = 0
        }
        this.changeTime = new Date()
        const imgPromise = new Promise((resolve, reject) => {
            var auxImg = new Image()
            auxImg.src = walk[0]
            auxImg.addEventListener("load", function () {
                if (auxImg.height != 0) {
                    resolve({ height: auxImg.height, width: auxImg.width })
                }
                else {
                    reject(auxImg.height)
                }
            })
        })
        imgPromise.then(result => {
            this.width = result.width
            this.height = result.height
            this.setY = (y - (this.height * 0.85));
            this.setX = x - this.width

        })
    }

    anime() {
        let now = new Date()
        let delta = now - this.changeTime

        if (this.roar) {
            //Roar
            if (delta > 500) {

                if (this.changeAnime) {
                    this.setImgSrc = roar[0]
                } else {
                    this.setImgSrc = roar[1]
                }

                this.changeAnime = !this.changeAnime
                this.changeTime = now

                delta = now - this.timeRoar
                if (delta > 1000) {
                    this.roar = false
                }
            }
        }
        else {
            if (delta > 500) {
                if (this.changeAnime) {
                    this.setImgSrc = walk[0]
                }
                else {
                    this.setImgSrc = walk[1]
                }
                this.changeAnime = !this.changeAnime
                this.changeTime = now
            }

        }
        //Roar Time
        now = new Date()
        delta = now - this.timeRoar
        if (delta > 6000) {
            this.roar = !this.roar
            this.timeRoar = now
        }
        
    }

    move(vel) {
        this.x += vel
    }

    collider(dino) {
        //Head
        return (dino.posX + 16 < this.x + 133 + 119 &&
            dino.posX + 16 + dino.w * .562 > this.x + 133 &&
            dino.posY < this.y + 12 + 89 &&
            dino.posY + dino.h * .869 > this.y + 12) ||
            //Body
            (dino.posX + 16 < this.x + 12 + 168 &&
                dino.posX + 16 + dino.w * .562 > this.x + 12 &&
                dino.posY < this.y + 103 + 69 &&
                dino.posY + dino.h * .869 > this.y + 103) ||
            //foot
            (dino.posX + 16 < this.x + 25 + 155 &&
                dino.posX + 16 + dino.w * .562 > this.x + 25 &&
                dino.posY < this.y + 173 + 96 &&
                dino.posY + dino.h * .869 > this.y + 173)

    }

}
