import Sky from './Obstacles.js'

var sun = "assets/sun.png"
export default class Sun extends Sky {
    constructor(x,y,cp){
        super()
        this.setImgSrc = sun
        this.setType = 'sun'
        this.setX = x

        if(cp){
            this.setY = y
        }
        else{
            const imgPromise = new Promise((resolve, reject) => {
                var auxImg = new Image()
                auxImg.src = sun
                auxImg.addEventListener("load", function () {
                    if (auxImg.height != 0) {
                        resolve(auxImg.height)
                    }
                    else {
                        reject(auxImg.height)
                    }
                })
            })
            imgPromise.then(result => {
                this.setY = (y - (result*5))

            })
        }
    }

}