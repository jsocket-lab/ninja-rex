import Obstacle from "./Obstacles.js"

var ImgPtero = [
    'assets/ptero1.png',
    'assets/ptero2.png']

export default class Ptero extends Obstacle {

    changeAnime = true
    constructor(x, y, cp) {
        super()
        this.setImgSrc = ImgPtero[0]
        this.setType = 'ptero'
        this.changeTime = new Date()
        this.setX = x
        if (cp) {
            this.setY = y
        } else {
            const imgPromise = new Promise((resolve, reject) => {
                var auxImg = new Image()
                auxImg.src = ImgPtero[0]
                auxImg.addEventListener("load", function () {
                    if (auxImg.height != 0) {
                        resolve(auxImg.height)
                    }
                    else {
                        reject(auxImg.height)
                    }
                })
            })
            imgPromise.then(result => {
                //this.iposY = (y - (result*0.75));
                this.randomHeight(y, result)
            })
        }
    }

    randomHeight(y, result) {
        let random = (Math.floor(Math.random() * (10 - 1 + 1)) + 1) % 3
        switch (random) {
            case 0:
                this.y = (y - (result * 0.7));
                break;
            case 1:
                this.y = (y - (result * 1.75));
                break;
            case 2:
                this.y = (y - (result * 2.5));
                break;
        }

    }
    anime() {
        let now = new Date()
        let delta = now - this.changeTime
        if (delta > 200) {
            if (this.changeAnime) {
                this.setImgSrc = ImgPtero[1]
            }
            else {
                this.setImgSrc = ImgPtero[0]
            }
            this.changeAnime = !this.changeAnime
            this.changeTime = now
        }

    }
}


