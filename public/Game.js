//Obstacles
import Ptero from './Ptero.js'
import SmallCactus from './SmallCactus.js'
import BigCactus from './BigCactus.js'
import Cactus from './Cactus.js'
import Cloud from './Cloud.js'
import Sun from './Sun.js'
import GigaRex from './Gigarex.js'
export default class Game {

    //Game Acceleration
    play = false
    vel = 5
    controlVar = 0

    //Mode
    rank = false
    gameOver = false
    obstaclesMode = true
    nightMode = false
    upsideDownMode = false
    gigarex = false

    //test collision
    remove = []

    //Time
    timer={
        obstacles:0,
        nightMode:0,
        gigarex:0,
        upsideDownMode:0
    }
    //lastObstacles = 0
    startObstacles = true

    //difficulty
    x = 1
    timeObstacles = 2000
    constructor() {
        this.players = []
        this.obstacles = []
        this.controls = []
        this.score = 0
        this.time = 0
        this.GameTime = new Date()
        this.sky = []
    }
    init(allPlayers, y) {
        //objects contruction  
        this.players = allPlayers
    }
    addSmallCactus(screenRes, y, index) {
        this.obstacles.push(new SmallCactus(screenRes, y, index))
    }
    addBigCactus(screenRes, y, index) {
        this.obstacles.push(new BigCactus(screenRes, y, index))
    }
    addCactus(screenRes, y) {
        this.obstacles.push(new Cactus(screenRes, y))
    }
    addPtero(screenRes, y, cp) {
        this.obstacles.push(new Ptero(screenRes, y, cp))
    }
    addGigarex(y,roar, anime) {
        this.obstacles.push(new GigaRex(0, y,roar, anime))
    }
    removeObstacles(obs) {
        this.obstacles.splice(this.obstacles.indexOf(obs), 1)
    }
    addPlayer(player){
        this.players.push(player)
    }
    addCloud(screenRes, y, cp) {
        this.sky.push(new Cloud(screenRes, y, cp))
    }
    addSun(screenRes,y,cp){
        this.sky.push(new Sun(screenRes,y,cp))
    }
    removeSky(sky) {
        this.sky.splice(this.sky.indexOf(sky), 1)
    }

    difficulty() {
        let now = new Date()
        let delta = now - this.GameTime

        if (delta > 1000) {
            if (this.vel < 11) {
                this.vel = Math.log(this.x) + 10
                if (this.timeObstacles > 1500)
                    this.timeObstacles -= this.timeObstacles * 0.01
                this.x += 0.1
                this.GameTime = now
            }
        }
    }

    reset() {
        this.players = []
        this.obstacles = []
        this.controls = []
        this.score = 0 
        this.time = 0
        this.GameTime = new Date()
        this.x = 1
        this.nightMode = false
        this.upsideDownMode = false
        this.gigarex = false
        this.rank = false
        this.gameOver = false
        this.timer={
            obstacles:0,
            nightMode:0,
            gigarex:0,
            upsideDownMode:0
        }
    }



}
