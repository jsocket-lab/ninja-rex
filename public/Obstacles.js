export default class Obstacle {

    img = new Image()
    height = this.img.height
    width = this.img.width
    dead = false
    type
    y
    x

    move(vel) {
        this.x -= vel
    }

    get getX() {
        return this.x
    }

    set setX(_newX) {
        this.x = _newX
    }

    get getY() {
        return this.y
    }

    set setY(_newY) {
        this.y = _newY
    }

    get getImgSrc() {
        return this.img.src
    }

    set setImgSrc(_newsrc) {
        this.img.src = _newsrc
    }

    get getType() {
        return this.type;
    }

    set setType(_newtype) {
        this.type = _newtype;
    }

    get getHeight() {
        this.height = this.img.height
        return this.height
    }

    get getWidth() {
        this.width = this.img.width
        return this.width
    }

}