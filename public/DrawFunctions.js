//Text
var text = [
    "PRESS THE GREEN BUTTON TO JOIN",
    "PRESS THE YELLOW BUTTON WHEN READY",
    "GAME OVER",
    "PRESS THE GREEN BUTTON TO RESTART",
    "HI"
]

var textRank = [
    "HI-SCORE",
    "RANK",
    "NAME",
    "SCORE"
]

var fakeRank = [
    "1ST", "---", "-----",
    "2ND", "---", "-----",
    "3RD", "---", "-----",
    "4TH", "---", "-----",
    "5TH", "---", "-----",
]

var newRecord = [
    "NEW RECORD",
    "-",
    "PRESS THE GREEN BUTTON TO NEXT",
    "PRESS THE YELLOW BUTTON TO CHANGE LETTER",
    "PRESS THE GREEN BUTTON TO FINISH",
]

let time = new Date()
let change = true
let color

export function drawText(ctx, screenW, screenH, i,cor) {
    if (cor)
        color = "white"
    else
        color = "black"
    ctx.font = '18px bit'
    ctx.textAlign = 'center'
    ctx.fillStyle = color
    ctx.fillText(text[i], screenW / 2, screenH / 4)
    ctx.fillText(text[i + 1], screenW / 2, screenH / 3)
}

export function drawRank(ctx, rank, screenW, screenH, i,cor) {
    if (cor)
        color = "white"
    else
        color = "black"
    ctx.font = '30px bit'
    ctx.textAlign = 'center'
    ctx.fillStyle = color
    ctx.fillText(textRank[i], screenW / 2, screenH / 14)
    ctx.font = '20px bit'
    ctx.fillText(textRank[i + 1], screenW / 4, screenH / 6)
    ctx.fillText(textRank[i + 2], screenW / 2, screenH / 6)
    ctx.fillText(textRank[i + 3], 3 * screenW / 4, screenH / 6)
    ctx.font = '18px bit'
    var j

    for (i = 0, j = 40; i < rank.length; i++ , j += 30) {
        ctx.fillText(fakeRank[i * 3], screenW / 4, (screenH / 6) + j)
        ctx.fillText(rank[i].name, screenW / 2, (screenH / 6) + j)
        ctx.fillText(rank[i].score, 3 * screenW / 4, (screenH / 6) + j)
    }
}

export function drawNewRecord(ctx, player, screenW, screenH, i, pos, cor) {
    if (cor)
        color = "white"
    else
        color = "black"
    ctx.font = '30px bit'
    ctx.textAlign = 'center'
    ctx.fillStyle = color
    ctx.fillText(newRecord[i], screenW / 2, screenH / 1.5)
    ctx.fillStyle = player.color
    let posW = screenW / 2
    let posH = 3 * screenH / 4
    for (var k = 0, j = 150; k < 3; k++ , j -= 40) {
        if (pos == k) {
            let now = new Date()
            let delta = now - time
            if (delta > 600) {
                change = !change
                time = now
            }
            if (change) {
                ctx.fillStyle = player.color
                ctx.fillText(player.letters[k], posW - j, posH)
            }
            else {
                ctx.fillStyle = 'white'
                ctx.fillText(player.letters[k], posW - j, posH)
            }

        } else {
            ctx.fillStyle = player.color
            ctx.fillText(player.letters[k], posW - j, posH)
        }
    }

    ctx.fillStyle = player.color
    ctx.fillText(newRecord[1], posW - 20, posH)
    ctx.fillText(player.number, posW + 90, posH)

    ctx.fillStyle = color
    ctx.font = '18px bit'
    if (pos > 2) {
        ctx.fillText(newRecord[4], posW, posH + 100)
    } else {
        ctx.fillText(newRecord[2], posW, posH + 100)
        ctx.fillText(newRecord[3], posW, posH + 140)
    }


}

export function drawHI(ctx, score, i, cor) {
    if (cor)
        ctx.fillStyle = "white"
    else
        ctx.fillStyle = "black"

    ctx.font = '25px bit'
    ctx.textAlign = 'start'
    ctx.fillText(text[i], 0, 25)
    for (var i = 0; i < 5; i++) {
        ctx.textAlign = "start";
        ctx.font = "25px bit";
        ctx.fillText(score, 50, 25);
    }
}

export function drawScore(ctx, screenW, score, y) {
    for (var i = 0; i < 5; i++) {
        ctx.font = "25px bit";
        ctx.textAlign = "end";
        ctx.fillStyle = score.color
        ctx.fillText(score.number, screenW, y);
    }
}